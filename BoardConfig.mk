#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Select the SoC
$(call set_soc, intel, baytrail)

# Select the kernel config
# This should be after the call to set_soc
$(call add_kernel_configs, $(realpath device/intel/minnowboard/minnowboard.kconf))

# Add wifi controller
$(call add_peripheral, intel, wifi/rtl8192cu)
# Add Broadcom USB BT controller
$(call add_peripheral, intel, bluetooth/btusbbcm)
# Add lights HAL
$(call add_peripheral, intel, light/mraa)
# Add audio support
$(call add_peripheral, intel, audio/generic/usb)
# Add sensor support
$(call add_peripheral, intel, sensors/mraa)
# Add framebuffer console support
$(call add_peripheral, intel, console/framebuffer)
# Add serial console support
$(call add_peripheral, intel, console/serial)
# Enable ADB-over-TCP on port 5555, started at boot
$(call enable_adb_over_tcp, 5555, true)
# Bootctrl HAL
$(call add_peripheral, intel, bootctrl)

BOARD_HAVE_BLUETOOTH_LINUX := true

BOARD_SYSTEMIMAGE_PARTITION_SIZE := 536870912
BOARD_BOOTLOADER_PARTITION_SIZE := 20971520
BOARD_USERDATAIMAGE_PARTITION_SIZE := 536870912
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_FLASH_BLOCK_SIZE := 512
TARGET_USERIMAGES_SPARSE_EXT_DISABLED := false

# Specific sensors to be built into the sensors HAL
PLATFORM_SENSOR_LIST += MPU9150Accelerometer
PLATFORM_SENSOR_LIST += MMA7660Accelerometer
PLATFORM_SENSOR_LIST += LSM303dAccelerometer

TARGET_BOOTLOADER_BOARD_NAME := $(TARGET_DEVICE)

#
# KERNELFLINGER Configuration
#
TARGET_UEFI_ARCH := x86_64
# Kernelflinger won't check the ACPI table oem_id, oem_table_id and
# revision fields
KERNELFLINGER_ALLOW_UNSUPPORTED_ACPI_TABLE := true
# Allow Kernelflinger to start watchdog prior to boot the kernel
KERNELFLINGER_USE_WATCHDOG := true
# Tell Kernelflinger to use pre-generated graphics resources
KERNELFLINGER_GEN_GRAPHICS := false
# Specify system verity partition
PRODUCT_SYSTEM_VERITY_PARTITION := /dev/block/by-name/system

BOOTLOADER_USE_PREBUILT := $(shell echo $${BOOTLOADER_USE_PREBUILT:-true})

# Note: We use := here to force the policy directory to be added
# on the first pass for make and to thus get around a build system bug.
BOARD_SEPOLICY_DIRS := $(BOARD_SEPOLICY_DIRS) device/intel/minnowboard/sepolicy

BOARD_BPT_INPUT_FILES += device/intel/minnowboard/device-partitions.bpt

vendor_partition_directory := vendor/bsp/intel/minnowboard

PRODUCT_COPY_FILES += \
    device/intel/minnowboard/flash_tools/brillo-flashall.sh:provision-device \
    device/intel/minnowboard/fstab:root/fstab.$(TARGET_DEVICE)

ifeq ($(BOOTLOADER_USE_PREBUILT),false)
PRODUCT_COPY_FILES += \
    $(vendor_partition_directory)/boot_binaries/oemvars.txt:oemvars.txt
endif

BRILLO_VENDOR_PARTITIONS := \
    $(vendor_partition_directory)/boot_binaries:bootloader \
    $(vendor_partition_directory)/boot_binaries:fastboot-usb.img \
    $(vendor_partition_directory)/boot_binaries:oemvars.txt \
    $(vendor_partition_directory)/boot_binaries:README

# Must defined at the end of the file
$(call add_device_packages)
